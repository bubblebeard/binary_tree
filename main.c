#include <stdio.h>
#include <stdlib.h>

typedef struct myitem {
    int data;
    struct myitem *left;
    struct myitem *right;
} node;

void insert(node**, int);
void remove_node(node**, int);
node* search(node*, int);
node* find_min(node*);
void show(node*, int);
void del_tree(node*);

int main(int argc, char** argv) {
    node *MyTree, *result;
    int value, choice;
    MyTree = NULL;
    while (1) {
        printf("select action:\n");
        printf("1. insert item\n");
        printf("2. remove item\n");
        printf("3. search\n");
        printf("4. show tree\n");
        printf("5. exit\n");
        scanf("%d", &choice);
        switch (choice) {
            case 1:
                printf("enter integer\n");
                scanf("%d", &value);
                printf("adding new node...\n");
                insert(&MyTree, value);
                break;
            case 2:
                printf("enter integer you want to remove\n");
                scanf("%d", &value);
                remove_node(&MyTree, value);
                break;
            case 3:
                printf("enter integer you want to find\n");
                scanf("%d", &value);
                result = search(MyTree, value);
                if (result == NULL) {
                    printf("node not found\n");
                } else {
                    printf("node exists\n");
                }
                break;
            case 4:
                show(MyTree, 0);
                printf("\n");
                break;
            case 5:
                del_tree(MyTree);
                exit(1);
            default:
                printf("unknown command\n");
                break;
        }
    }
    return (EXIT_SUCCESS);
}

void insert(node **tree, int value) {
    if (!(*tree)) {
        node *temp;
        temp = (node *) malloc(sizeof (node));
        temp->data = value;
        temp->left = temp->right = NULL;
        *tree = temp;
        printf("new node successfully added\n");
        return;
    } else
        if (value > (*tree)->data) {
        insert(&(*tree)->right, value);
    } else {
        if (value < (*tree)->data) {
            insert(&(*tree)->left, value);
        }
    }
    return;
}

void remove_node(node **tree, int value) {
    node *temp;
    if (!(*tree)) {
        printf("node not found\n");
        return;
    }
    if (value > (*tree)->data) {
        remove_node(&(*tree)->right, value);
    } else {
        if (value < (*tree)->data) {
            remove_node(&(*tree)->left, value);
        } else {
            if ((*tree)->left && (*tree)->right) {
                temp = find_min((*tree)->right);
                (*tree)->data = temp->data;
                remove_node(&(*tree)->right, temp->data);
            } else {
                temp = *tree;
                if ((*tree)->left == NULL)
                    *tree = (*tree)->right;
                else if ((*tree)->right == NULL)
                    *tree = (*tree)->left;
                free(temp);
            }
        }
    }
    return;
}

node* find_min(node *tree) {
    if (tree == NULL) {
        return NULL;
    }
    if (tree->left)
        return find_min(tree->left);
    else
        return tree;
}

node* search(node *tree, int value) {
    if (tree == NULL) {
        return NULL;
    }
    if (value > tree->data) {
        return search(tree->right, value);
    } else if (value < tree->data) {
        return search(tree->left, value);
    } else {
        return tree;
    }
}

void show(node *tree, int level) {
    if (tree) {
        show(tree->left, level + 1);
        for (int i = 0; i < level; i++) {
            printf("\t");
        }
        printf("%d\n", tree->data);
        show(tree->right, level + 1);
    }
}

void del_tree(node *tree) {
    if (tree) {
        del_tree(tree->left);
        del_tree(tree->right);
        free(tree);
    }
}